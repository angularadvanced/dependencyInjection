import { Injectable } from '@angular/core';
import { AppModule } from './app.module';

@Injectable({
  providedIn: 'root'
})
  export class LoggerService{

    private logs:string[]=[];

    logInfo(msg:string){
      this.log(msg)
    }

    logDebug(msg:string){
      this.log(msg)
    }

    logError(msg:string){
      this.log(msg)
    }

    private log(msg:string){
       this.logs.push(msg);
       console.log(msg);
    }
}

