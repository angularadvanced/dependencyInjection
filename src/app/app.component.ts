import { Component } from '@angular/core';
import { LoggerService } from './logger.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'dependecyInjection';
  //réutiliser le service
  constructor(private logger:LoggerService){}
   
  //tester la présence de l'instance de type LoggerService
  doLog(){
    this.logger.logInfo("un message de test-- LoggerService called")
  }
}
